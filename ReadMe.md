# Wpf.WindowDocking

## Overview

This is an example WPF application featuring windows docking capabilities with a dark theme (similar to Visual Studio).
This project was inspired by this [Tutorial]


## 3rd Party Libraries

- [Xceed WPF Toolkit] - Nuget
- [Monotone 1.4] - Included In project, with local edits
- [VS2013 Theme For Avalon Docks] - Included In Project, with local edits

## Nuget Packages

This project uses my [Common] library, which i bundle into NuGet Packages. These packages are available on my [Nuget Server]. You can also download and build the project yourself - replacing the NuGet references for file references.

## Known Issues (This list is still pretty long...)
- Exception Handling (Need Error Message Window)
- Properties view doesn't update when a new (or existing but edited) item is saved.
- Custom StyledWindow overlay has issues with transparency (it overlaps with the title when the page is small)
- PropertyGrid should open on Alphabetical View By Default, but its opening in category mode. (Probably due to the styling files) 
- AvalonDocks Dropdown Button remains unstyled
- Layout saving loading not working (Opens Documents on Load)
## Improvements Needed:
- Add FileController and applicable Interfaces
- Log4Net Debugging Window.
- Load Previous Layout Config on Application Start
- Add Proper Text Editor With Associated Styling (Something along the lines of AvalonEdit?)
- Toolbar
- Theme Swapper
- Settings Page (Feature Toggles / Theme etc.)
- Add Start Page (http://www.codeproject.com/Articles/483533/AvalonDock-Tutorial-Part-Adding-a-Start-Page)
- Design Data (Populate Main Window With Fake Views)
- Costura.Fody (Merge Dlls into Executable)


[Xceed WPF Toolkit]: http://wpftoolkit.codeplex.com/
[Tutorial]: http://www.codeproject.com/Articles/483507/AvalonDock-Tutorial-Part-Adding-a-Tool-Windo
[Monotone 1.4]: https://monotone.codeplex.com/
[VS2013 Theme For Avalon Docks]: https://github.com/4ux-nbIx/AvalonDock.Themes.VS2013
[Common]: https://gitlab.com/matthewamarlin/Common
[Nuget Server]: http://nuget.spearone.net