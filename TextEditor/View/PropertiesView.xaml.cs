﻿using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor.View
{
    /// <summary>
    /// Interaction logic for RecentFilesView.xaml
    /// </summary>
    public partial class PropertiesView
    {
        private static readonly ILogger Log = new Log4NetWrapper(typeof(PropertiesView));

        public PropertiesView()
        {
            InitializeComponent();
        }
    }
}
