﻿using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor.View
{
    /// <summary>
    /// Interaction logic for RecentFilesView.xaml
    /// </summary>
    public partial class RecentFilesView
    {
        private static readonly ILogger Log = new Log4NetWrapper(typeof(RecentFilesView));

        public RecentFilesView()
        {
            InitializeComponent();
        }
    }
}
