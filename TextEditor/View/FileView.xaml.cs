﻿using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;
using SpearOne.Examples.Wpf.AvalonDocks.TextEditor.ViewModel;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor.View
{
    /// <summary>
    /// Interaction logic for RecentFilesView.xaml
    /// </summary>
    public partial class FileView
    {
        private static readonly ILogger Log = new Log4NetWrapper(typeof(FileView));

        public FileView()
        {
            InitializeComponent();
        }
    }
}
