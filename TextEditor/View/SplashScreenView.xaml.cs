﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Media.Animation;
using System.Threading;
using System.Windows.Threading;
using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor.View
{
    /// <summary>
    /// Interaction logic for SplashScreen.xaml
    /// </summary>
    public partial class SplashScreenView
    {
        private static readonly ILogger Log = new Log4NetWrapper(typeof(SplashScreenView));

        public SplashScreenView()
        {
            InitializeComponent();
        }

        #region Fading Animation

        private bool _closeCompleted;

        private void WindowFadeOut_Completed(object sender, EventArgs e)
        {
            _closeCompleted = true;
            Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (_closeCompleted) return;
            WindowFadeOut.Begin();
            e.Cancel = true;
        }

        #endregion

        #region Focus

        private void Window_Deactivated(object sender, EventArgs e)
        {
            var window = (Window) sender;
            window.Topmost = true;
        }
        
        #endregion
    }
}