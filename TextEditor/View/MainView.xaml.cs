﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Input;
using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;
using SpearOne.Examples.Wpf.AvalonDocks.TextEditor.Common;
using SpearOne.Examples.Wpf.AvalonDocks.TextEditor.ViewModel;
using Xceed.Wpf.AvalonDock.Layout.Serialization;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor.View
{
    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView
    {
        private static readonly ILogger Log = new Log4NetWrapper(typeof(MainView));

        public const string LayoutConfigFile = @".\Layout.config";

        public MainView()
        {
            InitializeComponent();

            var mainViewModel = AppCompositionRoot.GetInstance<MainViewModel>();

            DataContext = mainViewModel;

            mainViewModel.InitCommandBinding(this);
        }

        #region LoadLayoutCommand

        private RelayCommand _loadLayoutCommand;
        public ICommand LoadLayoutCommand => _loadLayoutCommand ?? (_loadLayoutCommand = new RelayCommand(OnLoadLayout, CanLoadLayout));

        private static bool CanLoadLayout(object parameter)
        {
            return File.Exists(LayoutConfigFile);
        }

        private void OnLoadLayout(object parameter)
        {
            var layoutSerializer = new XmlLayoutSerializer(DockManager);

            layoutSerializer.LayoutSerializationCallback += (s, e) => {
                    //if (e.Model.ContentId == PropertiesViewModel.ToolContentId)
                    //    e.Content = Workspace.Instance.Properties;
                    //else if (!string.IsNullOrWhiteSpace(e.Model.ContentId) &&
                    //    File.Exists(e.Model.ContentId))
                    //    e.Content = Workspace.Instance.Open(e.Model.ContentId);
            };

            layoutSerializer.Deserialize(LayoutConfigFile);
        }

        #endregion

        #region SaveLayoutCommand

        private RelayCommand _saveLayoutCommand;
        public ICommand SaveLayoutCommand => _saveLayoutCommand ?? (_saveLayoutCommand = new RelayCommand(OnSaveLayout, CanSaveLayout));

        private static bool CanSaveLayout(object parameter)
        {
            return true;
        }

        private void OnSaveLayout(object parameter)
        {
            var layoutSerializer = new XmlLayoutSerializer(DockManager);
            layoutSerializer.Serialize(LayoutConfigFile);
        }

        #endregion

        private void OnDumpToConsole(object sender, RoutedEventArgs e)
        {
#if DEBUG
            DockManager.Layout.ConsoleDump(0);
#endif
        }
    }
}
