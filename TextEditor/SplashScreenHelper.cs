﻿using System;
using System.Threading;
using System.Windows.Threading;
using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;
using SpearOne.Examples.Wpf.AvalonDocks.TextEditor.View;
using SpearOne.Examples.Wpf.AvalonDocks.TextEditor.ViewModel;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor
{
    internal class SplashScreenHelper
    {
        private static readonly ILogger Log = new Log4NetWrapper(typeof(SplashScreenHelper));

        public static SplashScreenView SplashScreen { get; set; }

        public static void Show()
        {
            var thread = new Thread(delegate ()
               {
                   SplashScreen = new SplashScreenView
                   {
                       DataContext = new SplashScreenViewModel()
                   };
                   SplashScreen.Show();
                   Dispatcher.Run();                            
               });

            thread.SetApartmentState(ApartmentState.STA);
            thread.IsBackground = true;
            thread.Start();
            while (SplashScreen == null) { Thread.Sleep(1); }
        }

        public static void Hide()
        {
            if (SplashScreen == null) return;

            if (!SplashScreen.Dispatcher.CheckAccess())
            {
                var thread = new Thread(delegate (){
                    SplashScreen.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(delegate {
                                    SplashScreen.Close();
                        }));
                    });
                thread.SetApartmentState(ApartmentState.STA);
                thread.Start();
            }
            else
                SplashScreen.Hide();
        }

        public static void SetText(string text)
        {
            if (SplashScreen == null) return;

            if (!SplashScreen.Dispatcher.CheckAccess())
            {
                var thread = new Thread(
                    delegate ()
                    {
                        SplashScreen.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(delegate () {                            
                            ((SplashScreenViewModel) SplashScreen.DataContext).Text = text;
                                }
                            ));
                    });
                thread.SetApartmentState(ApartmentState.STA);
                thread.Start();
            }
            else
                ((SplashScreenViewModel)SplashScreen.DataContext).Text = text;
        }
    }
}
