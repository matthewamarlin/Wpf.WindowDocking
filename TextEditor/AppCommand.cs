﻿using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;
using System.Windows.Input;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor
{
    public class AppCommand
    {
        private static readonly ILogger Log = new Log4NetWrapper(typeof(AppCommand));

        #region Static Constructor (Constructs static application commands)
        /// <summary>
        /// Define custom commands and their key gestures
        /// </summary>
        static AppCommand()
        {
            var inputs = new InputGestureCollection();

            // Execute file open command (without user interaction)
            LoadFile = new RoutedUICommand("Open ...", "LoadFile", typeof(AppCommand), inputs);
        }
        #endregion Static Constructor

        #region CommandFramwork Properties (Exposes Commands to which the UI can bind to)

        /// <summary>
        /// Execute file open command (without user interaction)
        /// </summary>
        public static RoutedUICommand LoadFile { get; private set; }

        #endregion CommandFramwork_Properties
    }
}
