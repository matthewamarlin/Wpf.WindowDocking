﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using log4net.Appender;
using log4net.Core;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor.Common
{
    /// <summary>
    /// A Log4Net appender that logs values in memory and updates a WPF view.
    /// </summary>
    //TODO Make thread Safe
    public class LogViewAppender : AppenderSkeleton
    {
        public TextBox AppenderTextBox { get; set; }
        public string FormName { get; set; }
        public string TextBoxName { get; set; }
        
        protected override void Append(LoggingEvent loggingEvent)
        {
            var message = RenderLoggingEvent(loggingEvent);
        }
    }
}
