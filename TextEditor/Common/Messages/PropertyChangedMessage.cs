﻿using GalaSoft.MvvmLight.Messaging;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor.Common
{
    /// <summary>
    /// A MvvmLight Message to indicate that a bindable properties value has changed.
    /// </summary>
    public class PropertyChangedMessage : PropertyChangedMessageBase
    {
        public PropertyChangedMessage(object sender, object target, object oldValue, object newValue, string propertyName)
            : base(sender, target, propertyName)
        {
            OldValue = oldValue;
            NewValue = newValue;
        }

        public object NewValue { get; set; }
        public object OldValue { get; set; }
    }
}
