﻿using System;
using System.Windows.Input;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor.Common
{
    /// <summary>
    /// Used as a Fallback Value for Command Bindings. Provide a false value for the CanExecute method. 
    /// Throws an InvalidOperationException if the Execute method is called.
    /// </summary>
    public class NullCommand : ICommand
    {
        private static readonly Lazy<NullCommand> InternalInstance = new Lazy<NullCommand>(() => new NullCommand());

        private NullCommand()
        { }

        public event EventHandler CanExecuteChanged;

        public static ICommand Instance => InternalInstance.Value;

        public void Execute(object parameter)
        {
            throw new InvalidOperationException("NullCommand cannot be executed");
        }

        public bool CanExecute(object parameter)
        {
            return false;
        }
    }
}
