﻿namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor.ViewModel
{
    public class ViewModelLocator
    {
        public static MainViewModel Main => AppCompositionRoot.GetInstance<MainViewModel>();
        public static SplashScreenViewModel SplashScreen => AppCompositionRoot.GetInstance<SplashScreenViewModel>();
        public static FileViewModel File => AppCompositionRoot.GetInstance<FileViewModel>();
        public static PropertiesViewModel Properties => AppCompositionRoot.GetInstance<PropertiesViewModel>();
    }
}
