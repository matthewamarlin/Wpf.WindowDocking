﻿using System;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor.ViewModel.Interfaces
{
    internal interface IPaneViewModel
    {
        string Title { get; set; }
        Uri IconSource { get; set; }
        string ContentId { get; set; }
        bool IsSelected { get; set; }
        bool IsActive { get; set; }
    }
}
