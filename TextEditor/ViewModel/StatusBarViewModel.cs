﻿using System;
using SpearOne.Examples.Wpf.AvalonDocks.TextEditor.Messages;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor.ViewModel
{
    public class StatusBarViewModel : BaseViewModel
    {
        public StatusBarViewModel()
        {
            MessengerInstance.Register<ChangeStatusBarIconMessage>(this, ChangeStatusBarIconMessageHandler);
            MessengerInstance.Register<ChangeStatusBarTextMessage>(this, ChangeStatusBarTextMessageHandler);
        }

        #region Status Message

        public string Text { get; set; } = "Ready";

        private void ChangeStatusBarTextMessageHandler(ChangeStatusBarTextMessage message)
        {
            var text = message.Content.Text;
            Text = text;
        }

        #endregion

        #region Line Information

        public string LineNumberDisplayText { get; set; } = "Ln 0";
        public string ColumnNumberDisplayText { get; set; } = "Col 0";
        public string CharacterNumberDisplayText { get; set; } = "Ch 0";

        //TODO ?
        //private void ChangeLineInformationMessageHandler(ChangeStatusBarLineInformationMessage message)
        //{
        //    var uri = message.Content.IconSource;
        //    StatusIcon = uri;
        //}

        #endregion

        #region  Status Icon

        public Uri StatusIcon { get; set; } = new Uri("pack://application:,,,/SpearOne.Examples.Wpf.AvalonDocks.TextEditor;component/Images/Logo.png");

        private void ChangeStatusBarIconMessageHandler(ChangeStatusBarIconMessage message)
        {
            var uri = message.Content.IconSource;
            StatusIcon = uri;
        }
        
        #endregion
    }
}
