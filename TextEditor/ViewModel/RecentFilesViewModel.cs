﻿using System;
using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor.ViewModel
{
    public class RecentFilesViewModel : ToolViewModel
    {
        private static readonly ILogger Log = new Log4NetWrapper(typeof(RecentFilesViewModel));

        public RecentFilesViewModel()
          : base(nameof(RecentFilesViewModel), "Recent Files")
        {
            IconSource = new Uri("pack://application:,,,/SpearOne.Wpf.AvalonDocks.TextEditor;component/Images/Document.png", UriKind.RelativeOrAbsolute);
        }
    }
}
