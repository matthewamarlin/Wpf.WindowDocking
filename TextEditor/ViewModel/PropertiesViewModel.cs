﻿using System;
using System.IO;
using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;
using SpearOne.Examples.Wpf.AvalonDocks.TextEditor.Messages;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor.ViewModel
{
    public class PropertiesViewModel : ToolViewModel
    {
        private static readonly ILogger Log = new Log4NetWrapper(typeof(PropertiesViewModel));

        public dynamic SelectedObject { get; set; }

        public PropertiesViewModel()
          : base(nameof(PropertiesViewModel), "File Properties")
        {
            IconSource = new Uri("pack://application:,,,/SpearOne.Wpf.AvalonDocks.TextEditor;component/Images/Property-Blue.png", UriKind.RelativeOrAbsolute);
            MessengerInstance.Register<ActiveDocumentChangedMessage>(this, ActiveDocumentChangedMessageHandler);
        }

        private void ActiveDocumentChangedMessageHandler(ActiveDocumentChangedMessage message)
        {
            var ad = message.Content.ActiveDocument;
            if (ad?.FilePath != null && File.Exists(ad.FilePath))
            {
                var fi = new FileInfo(ad.FilePath);

                SelectedObject = fi;
            }
            else
            {
                SelectedObject = null;
            }
        }
    }
}
