﻿using System;
using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor.ViewModel
{
    public abstract class PaneViewModel : BaseViewModel
    {
        private static readonly ILogger Log = new Log4NetWrapper(typeof(PaneViewModel));

        protected PaneViewModel(string contentId, string title = null, Uri iconSource = null)
        {
            ContentId = contentId;
            Title = title;
            IconSource = iconSource;
        }

        public string ContentId { get; set; }
        public string Title { get; set; }
        public Uri IconSource { get; set; }
        public bool IsSelected { get; set; }
        public bool IsActive { get; set; }
    }
}
