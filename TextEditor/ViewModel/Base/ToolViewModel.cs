﻿using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor.ViewModel
{
    public class ToolViewModel : PaneViewModel
    {
        private static readonly ILogger Log = new Log4NetWrapper(typeof(PaneViewModel));

        public ToolViewModel(string contentId, string title, string name = null) : base(contentId)
        {
            Name = name ?? contentId.Replace("ViewModel", "Tool");
            Title = title;
        }

        public string Name { get; private set; }
        public bool IsVisible { get; set; }
    }
}
