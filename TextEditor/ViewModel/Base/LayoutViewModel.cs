﻿using System;
using System.IO;
using System.Windows.Input;
using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;
using SpearOne.Examples.Wpf.AvalonDocks.TextEditor.Common;
using Xceed.Wpf.AvalonDock;
using Xceed.Wpf.AvalonDock.Layout.Serialization;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor.ViewModel
{
    /// <summary>
    /// Class implements a viewmodel to support the
    /// <seealso cref="LayoutSerializer"/>
    /// attached behavior which is used to implement
    /// load/save of layout information on application
    /// start and shut-down.
    /// </summary>
    public class LayoutViewModel
    {
        private static readonly ILogger Log = new Log4NetWrapper(typeof(LayoutViewModel));

        #region Fields
        private RelayCommand _loadLayoutCommand;
        private RelayCommand _saveLayoutCommand;
        #endregion

        #region Command properties
        /// <summary>
        /// Implement a command to load the layout of an AvalonDock-DockingManager instance.
        /// This layout defines the position and shape of each document and tool window
        /// displayed in the application.
        /// 
        /// Parameter:
        /// The command expects a reference to a <seealso cref="DockingManager"/> instance to
        /// work correctly. Not supplying that reference results in not loading a layout (silent return).
        /// </summary>
        public ICommand LoadLayoutCommand
        {
            get
            {
                if (_loadLayoutCommand != null) return _loadLayoutCommand;

                _loadLayoutCommand = new RelayCommand(p =>
                {
                    var docManager = p as DockingManager;

                    if (docManager == null)
                        return;

                    LoadDockingManagerLayout(docManager);
                });

                return _loadLayoutCommand;
            }
        }

        /// <summary>
        /// Implements a command to save the layout of an AvalonDock-DockingManager instance.
        /// This layout defines the position and shape of each document and tool window
        /// displayed in the application.
        /// 
        /// Parameter:
        /// The command expects a reference to a <seealso cref="string"/> instance to
        /// work correctly. The string is supposed to contain the XML layout persisted
        /// from the DockingManager instance. Not supplying that reference to the string
        /// results in not saving a layout (silent return).
        /// </summary>
        public ICommand SaveLayoutCommand
        {
            get
            {
                return _saveLayoutCommand ?? (_saveLayoutCommand = new RelayCommand((p) =>
                       {
                           var xmlLayout = p as string;

                           if (xmlLayout == null)
                               return;

                           SaveDockingManagerLayout(xmlLayout);
                       }));
            }
        }
        #endregion

        #region Methods
        #region LoadLayout
        /// <summary>
        /// Loads the layout of a particular docking manager instance from persistence
        /// and checks whether a file should really be reloaded (some files may no longer
        /// be available).
        /// </summary>
        /// <param name="docManager"></param>
        private void LoadDockingManagerLayout(DockingManager docManager)
        {
            var layoutFileName = Path.Combine(MainViewModel.DirAppData, MainViewModel.LayoutFileName);

            if (File.Exists(layoutFileName) == false)
                return;

            var layoutSerializer = new XmlLayoutSerializer(docManager);

            layoutSerializer.LayoutSerializationCallback += (s, args) =>
            {
                    // This can happen if the previous session was loading a file
                    // but was unable to initialize the view ...
                    if (args.Model.ContentId == null)
                {
                    args.Cancel = true;
                    return;
                }

                LayoutViewModel.ReloadContentOnStartUp(args);
            };

            layoutSerializer.Deserialize(layoutFileName);
        }

        private static void ReloadContentOnStartUp(LayoutSerializationCallbackEventArgs args)
        {
            string sId = args.Model.ContentId;

            // Empty Ids are invalid but possible if aaplication is closed with File>New without edits.
            if (string.IsNullOrWhiteSpace(sId))
            {
                args.Cancel = true;
                return;
            }

            throw new NotImplementedException();
            //TODO
            //if (args.Model.ContentId == PropertiesViewModel.ToolContentId)
            //    args.Content = Workspace.This.FileStats;
            //else
            //{
                args.Content = ReloadDocument(args.Model.ContentId);

                if (args.Content == null)
                    args.Cancel = true;
            //}
        }

        private static object ReloadDocument(string path)
        {
            object ret = null;

            if (!string.IsNullOrWhiteSpace(path))
            {
                switch (path)
                {
                    /***
                              case StartPageViewModel.StartPageContentId: // Re-create start page content
                                if (Workspace.This.GetStartPage(false) == null)
                                {
                                  ret = Workspace.This.GetStartPage(true);
                                }
                                break;
                    ***/
                    default:
                        // Re-create text document
                        throw new NotImplementedException();
                        //TODO 
                        //ret = //Workspace.This.Open(path);
                        break;
                }
            }

            return ret;
        }
        #endregion

        #region SaveLayout
        private void SaveDockingManagerLayout(string xmlLayout)
        {
            // Create XML Layout file on close application (for re-load on application re-start)
            if (xmlLayout == null)
                return;

            var fileName = Path.Combine(MainViewModel.DirAppData, MainViewModel.LayoutFileName);

            File.WriteAllText(fileName, xmlLayout);
        }
        #endregion
        #endregion 
    }

}
