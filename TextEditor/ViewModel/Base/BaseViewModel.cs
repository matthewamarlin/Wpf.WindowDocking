﻿using GalaSoft.MvvmLight;
using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor.ViewModel
{
    public class BaseViewModel : ViewModelBase
    {
        private static readonly ILogger Log = new Log4NetWrapper(typeof(BaseViewModel));

        //protected virtual void Broadcast(object oldValue, object newValue, string propertyName)
        //{
        //    var message = new PropertyChangedMessage(this, this, oldValue, newValue, propertyName);
        //    if (MessengerInstance != null)
        //    {
        //        MessengerInstance.Send(message);
        //    }
        //    else
        //    {
        //        Messenger.Default.Send(message);
        //    }
        //}

        protected virtual void RaisePropertyChanged(string propertyName, object oldValue, object newValue)
        {
            // ReSharper disable once ExplicitCallerInfoArgument
            RaisePropertyChanged(propertyName);
            
            // Broadcast Message - This seems a bit excessive for every property change.
            // DispatcherHelper.CheckBeginInvokeOnUI(() => RaisePropertyChanged(propertyName));
            // Broadcast(oldValue, newValue, propertyName);
        }
    }
}
