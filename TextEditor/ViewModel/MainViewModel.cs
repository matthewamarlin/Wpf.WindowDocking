﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Microsoft.Win32;
using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;
using SpearOne.Examples.Wpf.AvalonDocks.TextEditor.Common;
using SpearOne.Examples.Wpf.AvalonDocks.TextEditor.Controllers.Interfaces;
using SpearOne.Examples.Wpf.AvalonDocks.TextEditor.Messages;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor.ViewModel
{
    public class MainViewModel : BaseViewModel
    {
        private static readonly ILogger Log = new Log4NetWrapper(typeof(MainViewModel));

        public MainViewModel()
        {
            _files = new ObservableCollection<FileViewModel>();
            Files = new ReadOnlyObservableCollection<FileViewModel>(_files);

            MessengerInstance.Register<CloseDocumentMessage>(this, CloseDocumentMessageHandler);
        }

        #region ViewModel Properties

        public FileViewModel ActiveDocument { get; set; }

        private readonly ObservableCollection<FileViewModel> _files;
        public ReadOnlyObservableCollection<FileViewModel> Files { get; }

        private PropertiesViewModel _properties;
        public PropertiesViewModel Properties => _properties ?? (_properties = new PropertiesViewModel());

        private ToolViewModel[] _tools;
        public IEnumerable<ToolViewModel> Tools => _tools ?? (_tools = new ToolViewModel[] { Properties });

        private LayoutViewModel _layout;
        /// <summary>
        /// Expose command to Load/Save the layout on application startup and shut-down.
        /// </summary>
        public LayoutViewModel Layout => _layout ?? (_layout = new LayoutViewModel());
        
        #endregion

        #region Fody Events

        public void OnActiveDocumentChanged()
        {
            Log.Write(Level.Trace, ActiveDocument.Title);
            MessengerInstance.Send(new ActiveDocumentChangedMessage(this, new ActiveDocumentChangedArgs(ActiveDocument)));
        }

        #endregion

        #region Message Handlers

        public void CloseDocumentMessageHandler(CloseDocumentMessage closeDocumentMessage)
        {
            Log.Write(Level.Trace, $"ContentId: {closeDocumentMessage.Content.Document.ContentId}, " +
                                   $"Title: {closeDocumentMessage.Content.Document.Title}");
            Close(closeDocumentMessage.Content.Document);
        }

        #endregion
        
        #region Commands

        #region OpenCommand

        private RelayCommand _openCommand;
        public ICommand OpenCommand => _openCommand ?? (_openCommand = new RelayCommand(OnOpen, CanOpen));

        private bool CanOpen(object parameter)
        {
            return true;
        }

        private void OnOpen(object parameter)
        {
            Open();
        }

        private void Open(string filepath = null)
        {
            if (filepath == null)
            {
                var dialog = new OpenFileDialog();
                var dialogHasFileResult = dialog.ShowDialog().GetValueOrDefault();
                if (dialogHasFileResult)
                {
                    filepath = dialog.FileName;
                }
            }

            var fileViewModel = AppCompositionRoot.GetInstance<FileViewModel>();

            //TODO Call a public method instead of manipulating properties? Or load using a constructor?
            fileViewModel.FilePath = filepath;

            ActiveDocument = fileViewModel;
        }

        #endregion

        #region CloseCommand

        private RelayCommand _closeCommand;
        public ICommand CloseCommand => _closeCommand ?? (_closeCommand = new RelayCommand(p => OnClose(), p => CanClose()));

        private bool CanClose()
        {
            return ActiveDocument != null;
        }

        private void OnClose()
        {
            Close(ActiveDocument);
        }

        public void Close(FileViewModel document)
        {
            if (document.IsDirty)
            {
                switch (MessageBox.Show($"Save changes for document '{document.FileName}'?", "Save Changes", MessageBoxButton.YesNoCancel))
                {
                    case MessageBoxResult.Cancel:
                        return;
                    case MessageBoxResult.Yes:
                        document.Save();
                        break;
                    case MessageBoxResult.None:
                        return;
                    case MessageBoxResult.OK:
                        document.Save();
                        break;
                    case MessageBoxResult.No:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            _files.Remove(document);

            if (document == ActiveDocument)
            {
                ActiveDocument = null;
            }
        }

        #endregion

        #region NewCommand

        private RelayCommand _newCommand;
        public ICommand NewCommand => _newCommand ?? (_newCommand = new RelayCommand(OnNew, CanNew));

        private static bool CanNew(object parameter)
        {
            return true;
        }

        private void OnNew(object parameter)
        {
            _files.Add(AppCompositionRoot.GetInstance<FileViewModel>());
            ActiveDocument = _files.Last();
        }

        #endregion

        #region ExitCommand

        private RelayCommand _exitCommand;
        public ICommand ExitCommand => _exitCommand ?? (_exitCommand = new RelayCommand(OnExit));

        private void OnExit(object parameter)
        {
            foreach (var file in _files)
            {
                Close(file);
            }

            //TODO Remove files from view model?

            Environment.Exit(0);
        }

        #endregion

        #region ToggleEditorOptionCommand
        private RelayCommand _toggleEditorOptionCommand;
        public ICommand ToggleEditorOptionCommand => _toggleEditorOptionCommand ?? (_toggleEditorOptionCommand = new RelayCommand(OnToggleEditorOption, CanToggleEditorOption));

        private bool CanToggleEditorOption(object parameter)
        {
            return ActiveDocument != null;
        }

        private void OnToggleEditorOption(object parameter)
        {
            var document = ActiveDocument;

            if (parameter == null)
                return;

            if ((parameter is ToggleEditorOption) == false)
                return;

            var option = (ToggleEditorOption) parameter;

            if (document == null) return; 

            switch (option)
            {
                case ToggleEditorOption.WordWrap:
                    document.WordWrap = !document.WordWrap;
                    break;

                case ToggleEditorOption.ShowLineNumber:
                    document.ShowLineNumbers = !document.ShowLineNumbers;
                    break;

                case ToggleEditorOption.ShowSpaces:
                    document.TextOptions.ShowSpaces = !document.TextOptions.ShowSpaces;
                    break;

                case ToggleEditorOption.ShowTabs:
                    document.TextOptions.ShowTabs = !document.TextOptions.ShowTabs;
                    break;

                case ToggleEditorOption.ShowEndOfLine:
                    document.TextOptions.ShowEndOfLine = !document.TextOptions.ShowEndOfLine;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        #endregion ToggleEditorOptionCommand

        /// <summary>
        /// Bind a window to some commands to be executed by the viewmodel.
        /// </summary>
        /// <param name="win"></param>
        public void InitCommandBinding(Window win)
        {
            win.CommandBindings.Add(new CommandBinding(AppCommand.LoadFile,
            (s, e) =>
            {
                var filename = e?.Parameter as string;

                if (filename == null)
                    return;

                Open(filename);
            }));
        }

        #endregion     

        #region Configuration Properties
        /// <summary>
        /// Get a path to the directory where the application
        /// can persist/load user data on session exit and re-start.
        /// </summary>
        public static string DirAppData
        {
            get
            {
                var dirPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + Path.DirectorySeparatorChar + Company;

                if (!Directory.Exists(dirPath))
                    Directory.CreateDirectory(dirPath);

                return dirPath;
            }
        }

        public static string Company => "SpearOne.TextEditor";
        public static string LayoutFileName => "Layout.config";

        #endregion Application Properties
    }
}
