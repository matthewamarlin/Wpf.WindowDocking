﻿using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor.ViewModel
{
    public class SplashScreenViewModel : BaseViewModel
    {
        private static readonly ILogger Log = new Log4NetWrapper(typeof(SplashScreenViewModel));

        public string Text { get; set; } = "Initializing...";
    }
}
