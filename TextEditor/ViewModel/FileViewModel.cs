﻿using System;
using System.IO;
using System.Windows.Input;
using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Highlighting;
using Microsoft.Win32;
using PropertyChanged;
using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;
using SpearOne.Examples.Wpf.AvalonDocks.TextEditor.Common;
using SpearOne.Examples.Wpf.AvalonDocks.TextEditor.Controllers.Interfaces;
using SpearOne.Examples.Wpf.AvalonDocks.TextEditor.Messages;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor.ViewModel
{
    [ImplementPropertyChanged]
    public class FileViewModel : PaneViewModel //, IViewModel<FileModel>
    {
        private static readonly ILogger Log = new Log4NetWrapper(typeof(FileViewModel));

        #region Connstructor

        public FileViewModel(IFileController fileController) : base(nameof(FileViewModel))
        {
            _fileController = fileController;

            IconSource = new Uri("pack://application:,,,/SpearOne.Wpf.AvalonDocks.TextEditor;component/Images/Document.png", UriKind.RelativeOrAbsolute);
            IsDirty = true;
        }

        #endregion

        #region Private Members

        private readonly IFileController _fileController;

        #endregion

        #region Public Properties

        // Hide inherited Title Property
        public new string Title => FileName + (IsDirty ? " *" : "");

        private string _filePath;
        public string FilePath
        {
            get { return _filePath; }
            set { _filePath = value; }
        }

        //this.HighlightDef = HighlightingManager.Instance.GetDefinition("XML");
        //this._isDirty = false;
        //this.IsReadOnly = false;
        //this.ShowLineNumbers = false;
        //this.WordWrap = false;

        [DependsOn(nameof(FilePath), nameof(IsDirty))]
        public string FileName => FilePath == null ? "Untitled.txt" : Path.GetFileName(FilePath);

        //public string TextContent { get; set; }
        public TextDocument Document { get; set; }

        public bool IsDirty { get; set; }

        public bool ShowLineNumbers { get; set; } = false;

        public TextEditorOptions TextOptions { get; set; } = new TextEditorOptions { ConvertTabsToSpaces = false, IndentationSize = 2 };

        public IHighlightingDefinition HighlightDef { get; set; }

        public bool WordWrap { get; set; } = true;

        public bool IsReadOnly { get; set; } = false;

        #endregion
        
        #region Fody Events

        public void OnTitleChanged()
        {
            Log.Write(Level.Trace, nameof(OnTitleChanged));
        }
        
        public void OnFilePathChanged()
        {
            Log.Write(Level.Trace, nameof(OnFilePathChanged));
        }

        private void OnTextContentChanged()
        {
            Log.Write(Level.Trace, nameof(OnTextContentChanged));

            //if (IsDirty) return;

            //if (!File.Exists(FilePath))
            //{
            //    IsDirty = TextContent != File.ReadAllText(FilePath);
            //}
            //else
            //{
            //    IsDirty = true;
            //}
        }

        public void OnIsDirtyChanged()
        {
            Log.Write(Level.Trace, nameof(OnIsDirtyChanged));
        }

        #endregion

        #region Commands

        #region SaveCommand

        private RelayCommand _saveCommand;
        public ICommand SaveCommand => _saveCommand ?? (_saveCommand = new RelayCommand(OnSave, CanSave));

        private bool CanSave(object parameter)
        {
            return IsDirty;
        }

        private void OnSave(object parameter)
        {
            Save();
        }

        #endregion

        #region SaveAsCommand

        private RelayCommand _saveAsCommand;
        public ICommand SaveAsCommand => _saveAsCommand ?? (_saveAsCommand = new RelayCommand(OnSaveAs, CanSaveAs));

        private bool CanSaveAs(object parameter)
        {
            return IsDirty;
        }

        private void OnSaveAs(object parameter)
        {
            Save(true);
        }
        
        public void Save(bool saveAs = false)
        {
            //TODO Make your own dialog?
            if (FilePath == null || saveAs)
            {
                var dialog = new SaveFileDialog();
                if (dialog.ShowDialog().GetValueOrDefault())
                    FilePath = dialog.FileName;
            }

            if (FilePath == null) return;

            _fileController.Save(Document);
            IsDirty = false;

            //TODO Update FileInfo Object???
        }

        #endregion

        #region CloseCommand

        private RelayCommand _closeCommand;

        public ICommand CloseCommand => _closeCommand ?? (_closeCommand = new RelayCommand(p => OnClose(), p => CanClose()));

        private bool CanClose() => true;

        private void OnClose()
        {
            MessengerInstance.Send(new CloseDocumentMessage(this, new CloseDocumentMessageArgs(this)));
        }

        #endregion

        #endregion
    }
}
