﻿using System;
using System.IO;
using System.Text;
using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Utils;
using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;
using SpearOne.Examples.Wpf.AvalonDocks.TextEditor.Controllers.Interfaces;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor.Controllers
{
    //TODO Improve This Shit!!!
    public class FileController : IFileController
    {
        private static readonly ILogger Log = new Log4NetWrapper(typeof(FileController));

        public TextDocument Load(string filePath)
        {
            Log.Write(Level.Trace, filePath);

            if (!File.Exists(filePath))
            {
                Log.Write(Level.Warn, $"'{filePath}' could not be found.");
                throw new FileNotFoundException(filePath);
            }

            //// Check file attributes and set to read-only if file attributes indicate that
            //if ((System.IO.File.GetAttributes(this._filePath) & FileAttributes.ReadOnly) != 0)
            //{
            //    this.IsReadOnly = true;
            //    this.IsReadOnlyReason = "This file cannot be edit because another process is currently writting to it.\n" +
            //        "Change the file access permissions or save the file in a different location if you want to edit it.";
            //}

            using (var fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                using (var streamReader = FileReader.OpenStream(fs, Encoding.UTF8))
                {
                    return new TextDocument(streamReader.ReadToEnd());
                }
            }
        }

        public void Save(TextDocument document)
        {
            Log.Write(Level.Trace, document.FileName);
            
            //TODO Check if path is valid

            File.WriteAllText(document.FileName, document.Text);
        }
    }
}
