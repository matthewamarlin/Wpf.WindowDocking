﻿using ICSharpCode.AvalonEdit.Document;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor.Controllers.Interfaces
{
    public interface IFileController
    {
        TextDocument Load(string filePath);
        void Save(TextDocument document);
    }
}