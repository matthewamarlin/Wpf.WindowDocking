﻿using System;
using System.Threading;
using GalaSoft.MvvmLight.Threading;
using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;
using SpearOne.Examples.Wpf.AvalonDocks.TextEditor.View;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor
{
    internal static class Program
    {
        private static readonly ILogger Log = new Log4NetWrapper(typeof(Program));

        [STAThread]
        public static void Main()
        {
            try
            {
                SplashScreenHelper.Show();

                SplashScreenHelper.SetText("Loading Plugins");
                Thread.Sleep(2000);
                
                var app = new App();
                app.InitializeComponent();
                
                AppCompositionRoot.Initialize();
                DispatcherHelper.Initialize();

                SplashScreenHelper.SetText(string.Empty);
                Thread.Sleep(500);
                SplashScreenHelper.Hide();

                var mainWindow = AppCompositionRoot.GetInstance<MainView>();
                
                //Needed for Shutdown OnMainWindowClose in App.xaml
                app.MainWindow = mainWindow;
                app.Run(mainWindow);

                Log.Write(Level.Trace, "Application Shutdown");
            }
            catch (Exception exception)
            {
                Log.WriteData(Level.Fatal,
                    Tuple.Create("Error Messages", exception.Message),
                    Tuple.Create("InnerException Message", exception.InnerException?.Message),
                    Tuple.Create("Stack Trace", exception.StackTrace));
#if DEBUG
                Console.WriteLine(@"Press any key to exit...");
                Console.ReadKey();
#endif
            }
        }
    }
}
