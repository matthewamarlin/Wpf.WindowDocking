﻿using System;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor.Messages
{
    public class ChangeStatusBarIconMessageArgs
    {
        public Uri IconSource { get; set; }

        public ChangeStatusBarIconMessageArgs(Uri iconSource)
        {
            IconSource = iconSource;
        }
    }
}
