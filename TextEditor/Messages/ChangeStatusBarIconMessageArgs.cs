﻿namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor.Messages
{
    public class ChangeStatusBarTextMessageArgs
    {
        public string Text { get; set; }

        public ChangeStatusBarTextMessageArgs(string text)
        {
            Text = text;
        }
    }
}
