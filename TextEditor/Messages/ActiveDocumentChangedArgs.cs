﻿using SpearOne.Examples.Wpf.AvalonDocks.TextEditor.ViewModel;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor.Messages
{
    public class ActiveDocumentChangedArgs
    {
        public FileViewModel ActiveDocument { get; set; }

        public ActiveDocumentChangedArgs(FileViewModel activeDocument)
        {
            ActiveDocument = activeDocument;
        }
    }
}
