﻿using GalaSoft.MvvmLight.Messaging;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor.Messages
{
    public class CloseDocumentMessage : GenericMessage<CloseDocumentMessageArgs>
    {
        public CloseDocumentMessage(object sender, CloseDocumentMessageArgs content) : base(sender, content)
        { }
    }
}
