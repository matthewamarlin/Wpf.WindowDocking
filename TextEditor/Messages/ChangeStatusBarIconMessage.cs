﻿using GalaSoft.MvvmLight.Messaging;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor.Messages
{
    public class ChangeStatusBarIconMessage : GenericMessage<ChangeStatusBarIconMessageArgs>
    {
        public ChangeStatusBarIconMessage(object sender, ChangeStatusBarIconMessageArgs content) : base(sender, content)
        { }
    }
}
