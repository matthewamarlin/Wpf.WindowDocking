﻿using GalaSoft.MvvmLight.Messaging;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor.Messages
{
    public class ActiveDocumentChangedMessage : GenericMessage<ActiveDocumentChangedArgs>
    {
        public ActiveDocumentChangedMessage(object sender, ActiveDocumentChangedArgs content) : base(sender, content)
        { }
    }
}
