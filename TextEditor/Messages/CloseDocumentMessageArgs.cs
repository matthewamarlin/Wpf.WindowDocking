﻿using SpearOne.Examples.Wpf.AvalonDocks.TextEditor.ViewModel;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor.Messages
{
    public class CloseDocumentMessageArgs
    {
        public FileViewModel Document { get; set; }

        public CloseDocumentMessageArgs(FileViewModel document)
        {
            Document = document;
        }
    }
}
