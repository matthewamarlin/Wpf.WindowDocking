﻿using GalaSoft.MvvmLight.Messaging;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor.Messages
{
    public class ChangeStatusBarTextMessage : GenericMessage<ChangeStatusBarTextMessageArgs>
    {
        public ChangeStatusBarTextMessage(object sender, ChangeStatusBarTextMessageArgs content) : base(sender, content)
        { }
    }
}
