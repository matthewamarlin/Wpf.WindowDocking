﻿using System.Windows;
using System.Windows.Controls;
using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;
using SpearOne.Examples.Wpf.AvalonDocks.TextEditor.ViewModel;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor.Layout
{
    internal class PanesTemplateSelector : DataTemplateSelector
    {
        private static readonly ILogger Log = new Log4NetWrapper(typeof(PanesTemplateSelector));

        public DataTemplate FileViewTemplate { get; set; }

        public DataTemplate RecentFilesViewTemplate { get; set; }

        public DataTemplate FileStatsViewTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item is FileViewModel)
                return FileViewTemplate;

            if (item is PropertiesViewModel)
                return FileStatsViewTemplate;

            if (item is RecentFilesViewModel)
                return RecentFilesViewTemplate;

            return base.SelectTemplate(item, container);
        }
    }
}
