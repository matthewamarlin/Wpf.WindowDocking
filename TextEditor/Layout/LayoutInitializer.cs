﻿using System.Linq;
using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;
using SpearOne.Examples.Wpf.AvalonDocks.TextEditor.View;
using Xceed.Wpf.AvalonDock.Layout;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor.Layout
{
    internal class LayoutInitializer : ILayoutUpdateStrategy
    {
        private static readonly ILogger Log = new Log4NetWrapper(typeof(LayoutInitializer));

        public bool BeforeInsertAnchorable(LayoutRoot layout, LayoutAnchorable anchorableToShow, ILayoutContainer destinationContainer)
        {
            //Set how the layout shuold be initialized

            //var destPane = destinationContainer as LayoutAnchorablePane;
            //if (destinationContainer?.FindParent<LayoutFloatingWindow>() != null)
            //    return false;

            //var toolsPane = layout.Descendents().OfType<LayoutAnchorablePane>().FirstOrDefault(d => d.Name == "ToolsPane");

            //if (toolsPane != null)
            //{
            //    anchorableToShow.IsActive = true;

            //    toolsPane.Children.Add(anchorableToShow);
            //    return true;
            //}

            return false;
        }

        public void AfterInsertAnchorable(LayoutRoot layout, LayoutAnchorable anchorableShown)
        {
        }


        public bool BeforeInsertDocument(LayoutRoot layout, LayoutDocument anchorableToShow, ILayoutContainer destinationContainer)
        {
            return false;
        }

        public void AfterInsertDocument(LayoutRoot layout, LayoutDocument anchorableShown)
        {

        }
    }
}
