﻿using System.Windows;
using System.Windows.Controls;
using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;
using SpearOne.Examples.Wpf.AvalonDocks.TextEditor.ViewModel;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor.Layout
{
    internal class PanesStyleSelector : StyleSelector
    {
        private static readonly ILogger Log = new Log4NetWrapper(typeof(PanesStyleSelector));

        public Style ToolStyle { get; set; }

        public Style FileStyle { get; set; }

        public Style RecentFilesStyle { get; set; }

        public override Style SelectStyle(object item, DependencyObject container)
        {
            if (item is RecentFilesViewModel)
                return RecentFilesStyle;

            if (item is ToolViewModel)
                return ToolStyle;

            if (item is FileViewModel)
                return FileStyle;

            return base.SelectStyle(item, container);
        }
    }
}
