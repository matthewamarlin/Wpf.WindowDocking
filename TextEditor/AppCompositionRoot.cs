﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows;
using log4net.Config;
using SimpleInjector;
using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;
using SpearOne.Examples.Wpf.AvalonDocks.TextEditor.Controllers;
using SpearOne.Examples.Wpf.AvalonDocks.TextEditor.Controllers.Interfaces;
using SpearOne.Examples.Wpf.AvalonDocks.TextEditor.View;
using SpearOne.Examples.Wpf.AvalonDocks.TextEditor.ViewModel;

namespace SpearOne.Examples.Wpf.AvalonDocks.TextEditor
{
    public class AppCompositionRoot
    {
        private static readonly ILogger Log = new Log4NetWrapper(typeof(AppCompositionRoot));

        private const string ContainerNotSetMessage = "The dependency injection container has not been initialised. Please " +
                                                      "call the Initilize method on the " + nameof(AppCompositionRoot) + "before requesting any types.";
        private static Container _container;

        public static void Initialize()
        {
            try
            {
                _container = new Container();
                
                //Log4Net 
                XmlConfigurator.Configure();
                Log.Write(Level.Info, $"{Assembly.GetExecutingAssembly().GetName().Name } Initialising");

                // Business Layer 
                _container.Register<IFileController, FileController>();

                // Open Generic
                var assembly = Assembly.GetExecutingAssembly();
                var assemblies = new List<Assembly> { assembly };

                //_container.Register(typeof(IView<>), assemblies);
                //_container.Register(typeof(IViewModel<>), assemblies);
                //_container.Register(typeof(IViewProvider<,>), assemblies);
                //_container.Register(typeof(IModelProvider<>), assemblies);

                // Models
                //_container.Register<FileModel>();

                //Register ViewModels
                _container.Register<FileViewModel>();
                _container.Register<MainViewModel>();
                _container.Register<SplashScreenViewModel>();

                //Register Views
                _container.Register<FileView>();
                _container.Register<MainView>();
                _container.Register<SplashScreenView>();

                _container.Verify();
            }
            catch (Exception exception)
            {
                throw new Exception($"An erorr occured when attempting to initialise the service: {exception.Message}");
            }
        }

        public static T GetInstance<T>() where T : class
        {
            if (_container == null)
                throw new InvalidOperationException(ContainerNotSetMessage);

            return _container.GetInstance<T>();
        }

        public static object GetInstance(Type serviceType)
        {
            if (_container == null)
                throw new InvalidOperationException(ContainerNotSetMessage);

            return _container.GetInstance(serviceType);
        }
    }
}
